vim.keymap.set("n", "<leader>nh", ":nohlsearch<CR>")

-- nvim-tree
vim.keymap.set("n", "<leader>e", "<cmd>:NvimTreeFindFileToggle<CR>")

vim.keymap.set("n", "<leader>l", "<cmd>:Lazy<CR>")
