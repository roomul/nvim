return {
  "stevearc/dressing.nvim",
  opts = {},
  config = function()
    require("dressing").setup({
      input = {
        enabled = true,
      },
      select = {
        enabled = true,
      },
    })
  end
}
