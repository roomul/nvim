return {
  "goolord/alpha-nvim",
  dependencies = {
    "nvim-tree/nvim-web-devicons"
  },
  config = function()
    local alpha = require("alpha")
    local dashboard = require("alpha.themes.dashboard")
    dashboard.section.header.val = {
      "                                                     ",
      "  ███╗   ██╗███████╗ ██████╗ ██╗   ██╗██╗███╗   ███╗ ",
      "  ████╗  ██║██╔════╝██╔═══██╗██║   ██║██║████╗ ████║ ",
      "  ██╔██╗ ██║█████╗  ██║   ██║██║   ██║██║██╔████╔██║ ",
      "  ██║╚██╗██║██╔══╝  ██║   ██║╚██╗ ██╔╝██║██║╚██╔╝██║ ",
      "  ██║ ╚████║███████╗╚██████╔╝ ╚████╔╝ ██║██║ ╚═╝ ██║ ",
      "  ╚═╝  ╚═══╝╚══════╝ ╚═════╝   ╚═══╝  ╚═╝╚═╝     ╚═╝ ",
      "                                                     ",
    }
    dashboard.section.buttons.val = {
      dashboard.button("e", " New file", ":ene <BAR> startinsert <CR>"),
      dashboard.button("space space", "󰛔 Find file", ":Telescope find_files <CR>"),
      dashboard.button("space e", "󰪶 File browser", ":NvimTreeOpen<CR>"),
      dashboard.button("t", "󰈞 Find text", ":Telescope live_grep <CR>"),
      dashboard.button("c", " Configuration", ":e ~/.config/nvim<CR>"),
      dashboard.button("space l", " Lazy: package manager", ":Lazy<CR>"),
      dashboard.button("q", "󰗼 Quit Neovim", ":qa<CR>"),
    }

    local function footer()
      return "Don't Stop Coding..."
    end
    dashboard.section.footer.val = footer()
    
    alpha.setup(dashboard.opts)
  end
}
